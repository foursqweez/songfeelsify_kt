package com.songfeelsfinal.songfeels;

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.songfeelsfinal.songfeels.ui.fragment.HomeFragment
import com.songfeelsfinal.songfeels.ui.fragment.ProfileFragment
import com.songfeelsfinal.songfeels.ui.insertPhoto.InsertPhotoActivity
import com.songfeelsfinal.songfeels.ui.showEmotion.CustomDialog
import com.songfeelsfinal.songfeels.ui.spotify.BaseActivity
import com.songfeelsfinal.songfeels.ui.spotify.CallbackBaseActivityFinished
import kotlinx.android.synthetic.main.activity_main.*


@Suppress("UNREACHABLE_CODE")
class MainActivity : BaseActivity(),
        CallbackBaseActivityFinished,
        CustomDialog.CustomGenreDialogListener, CustomDialog.CustomDialogListener,
        CustomDialog.CustomSavePlaylistDialogListener {

    var checkLogIn: Boolean? = null
    lateinit var sp: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var loadProgress: ProgressBar? = null
    private lateinit var fragment: Fragment


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_home -> {
                fragment = HomeFragment()
                loadFragment(fragment)
                return@OnNavigationItemSelectedListener true

            }

            R.id.navigation_camera -> {
                val intent = Intent(this, InsertPhotoActivity::class.java)
                startActivity(intent)
            }

            R.id.navigation_profile -> {
                fragment = ProfileFragment()
                loadFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        loadProgress?.visibility = View.VISIBLE

        sp = getSharedPreferences("Pref_CheckLogin", Context.MODE_PRIVATE)
        checkLogIn = sp.getBoolean("Check", true);

    }


    var doubleBackToExitPressedOnce: Boolean = false

    @Override
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            onDestroy();
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable {
            kotlin.run {
                doubleBackToExitPressedOnce = false
            }
        }, 2000)
    }

    private fun openDialog() {

        val customDialog = CustomDialog("Genre")
        customDialog.show(supportFragmentManager, "Genre Selection");
    }

    private fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit()

            return true
        }
        return false
    }


    override fun OnBaseActivityLoadFinished(baseLoadFinished: String?) {
        val preferences: SharedPreferences = getSharedPreferences("myPrefs", MODE_PRIVATE)
        if (preferences.getBoolean("firstLogin", true)) {
            openDialog()
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putBoolean("firstLogin", false)
            editor.apply()
            loadFragment(HomeFragment())
        } else {
            loadFragment(HomeFragment())
        }
        loadProgress?.visibility = View.GONE;
    }


    override fun applyTextsCustomTime(customTime: String?) {
    }

    override fun applyTextsListTime(listTime: String?) {
    }

    override fun applyTextSavePlaylist(savePlaylistName: String?) {
    }

    override fun applyTextSavePlaylistUserPlaylistID(savePlaylistUserPlaylistID: String?, playlistIdSpotify: String?) {
    }

    override fun applyTextsListGenre(listGenre: String?) {

    }
}


